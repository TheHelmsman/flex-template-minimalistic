/**
 * Created with IntelliJ IDEA.
 * User: Mikromafia
 * Date: 14/05/13
 * Time: 20:18
 * To change this template use File | Settings | File Templates.
 */
package com.petrasplanet.templateflex {

	import flash.events.MouseEvent;
	import mx.controls.Alert;
	import mx.events.FlexEvent;
	import spark.components.Application;
	import spark.components.Button;

	public class CustomApplication extends Application {

		public function CustomApplication() {

			addEventListener(FlexEvent.CREATION_COMPLETE, creationHandler);
		}

		private function creationHandler(e:FlexEvent):void {

			var button:Button = new Button();
			button.label = "My favorite button";
			button.styleName = "halo";
			button.addEventListener(MouseEvent.CLICK, handleClick);
			addElement(button);
		}

		private function handleClick(e:MouseEvent):void {

			Alert.show("You clicked it!", "Clickity!");
		}
	}
}
